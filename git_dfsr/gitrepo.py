import git
import logging
import os, stat

from .config import Config
from .error import Error


class Prepender:
    """
    Convenience class for prepending a file.
    """

    def __init__(self, fname, mode='r+'):
        self.__write_queue = []
        self.__f = open(fname, mode)

    def write(self, s):
        self.__write_queue.insert(0, s)

    def close(self):
        self.__exit__(None, None, None)

    def __enter__(self):
        return self

    def __exit__(self, type, value, traceback):
        if self.__write_queue:
            self.__f.writelines(self.__write_queue)
        self.__f.close()

# with Prepender('test.out') as f:
#     f.write('string 3\n')
#     f.write('string 2\n')
#     f.write('string 1\n')


def get_node_id():
    """
    Returns the node id from the config object or a default if None.
    """
    return str(Config.get('local', 'node_id')) or '1'

class GitRepo(object):
    """
    Interact with a local git repository and its remotes.
    """

    def __init__(self, path, remote, key=None):
        # path to the git repo locally:
        self.path = path
        # url to the git remote:
        self.remote = remote
        self.get_remote_pieces()
        # create the GitPython git repo object:
        self.repo = git.Repo(path)
        # add an alias remote with the ssh key passed if it exists:
        if key:
            self.setup_ssh_conf(key)
        # add a new remote with the ssh key:
        self.add_remote()
        # initial sync, to recover from reboots/outages.
        self.local_changes()

    def get_remote_pieces(self):
        """
        Break the remote url into its parts.
        """
        if not '@' in self.remote or not ':' in self.remote.split('@')[1]\
                or not '/' in self.remote.split('@')[1].split(':')[1]:
            raise Error('Unexpected format of remote URL; expected format is: git@<remote_host>:<user>/<repo>.git')
        self.remote_host = self.remote.split('@')[1].split(':')[0]
        self.remote_tail = self.remote.split('@')[1].split(':')[1]
        self.remote_user = self.remote_tail.split('/')[0]
        self.remote_repo = self.remote_tail.split('/')[1]

    def add_remote(self):
        """
        Add a remote with a special name for use with the ssh config file.
        """
        # first, remove any dfsr remote to be safe
        try:
            self.repo.git.remote('rm', 'dfsr')
        except:
            # swallow the exception for now, though we may find a need to handle specific errors later.
            pass
        self.repo.git.remote('add', 'dfsr', 'git@dfsr:'+self.remote_tail)

    def setup_ssh_conf(self, key):
        """
        Create a new ~/.ssh/config or update the existing one with the ssh key for this remote.
        """
        conf_path = os.path.join(os.path.expanduser("~"),'.ssh','config')
        # if .ssh config doesn't already exist, create it:
        if not os.path.exists(conf_path):
            # set permissions 0600 on the new file:
            mode = stat.S_IRUSR | stat.S_IWUSR  # This is 0o600 in octal and 384 in decimal.
            umask_original = os.umask(0)
            try:
                handle = os.fdopen(os.open(conf_path, os.O_WRONLY | os.O_CREAT, mode), 'w')
            finally:
                os.umask(umask_original)
            handle.close()
        found = False
        with file(conf_path, 'r+') as f:
            line = f.readline()
            if 'Host dfsr' in line:
                found = True
        #         self.update_ssh_entry(conf_path)
        # for now, only updating the ssh config if there is not an existing dfsr entry. This will actually
        # break for multiple installs of dfsr on the same host for different git remotes. Nevertheless, it
        # means rerunning dfsr with the same remote does not result in a massive config file over
        # time. The real solution is to update the existing entry if necessary: it's on the list.
        if not found:
            with Prepender(conf_path) as f:
                f.write('  IdentitiesOnly yes' + '\n')
                f.write('  IdentityFile ' + key + '\n')
                f.write('  User git\n')
                f.write('  HostName ' + self.remote_host + '\n')
                f.write('Host dfsr\n')
        #     self.add_ssh_entry(conf_path)

    def update_ssh_entry(self, path):
        """
        Update an ssh config file that already has a dfsr host entry.
        """
        # todo
        pass
        # from tempfile import mkstemp
        # from shutil import move
        # fh, abs_path = mkstemp()
        # new_file = open(abs_path,'w')
        # old_file = open(path)
        # for line in old_file:
        #     if 'Host dfsr' in line:
        #         old_file.readline()
        # #close temp file
        # new_file.close()
        # os.close(fh)
        # old_file.close()
        # #Remove original file
        # os.remove(path)
        # #Move new file
        # move(abs_path, path)


    def local_changes(self):
        """
        Convenience function for managing local changes to a git repository.
        """
        logging.info("Processing local changes")

        # check status of local repository
        out = self.repo.git.status()
        if 'working directory clean' in out:
            return

        # add all changes
        out = self.repo.git.add(A=True)
        if out:
            logging.warning('Unexpected result attempting to execute git add: ' + str(out))

        # commit changes
        out = self.repo.git.commit(m=get_node_id())
        if 'error' in out:
            logging.error('Error making commit; stdout: ' + str(out))
            raise Error('Error making commit; stdout: ' + str(out))

        # fetch and merge from origin -- it is required to fetch and merge before pushing in case previous POSTs
        # failed.
        self.fetch_and_merge()

        # push local master to origin
        logging.info("about to push to remote; path: " + str(self.path))
        out = self.repo.git.push('dfsr', 'master', u=True)
        logging.info("Pushed local master to origin from node: " + str(get_node_id()) + ". stdout: " + str(out))

    def fetch_and_merge(self):
        """
        Fetch from the git remote and merge using the recursive strategy with 'theirs' option for conflict resolution.
        """
        out = self.repo.git.fetch('dfsr')
        self.repo.git.merge('dfsr/master', s='recursive', X='theirs')


    def remote_changes(self):
        """
        Convenience function for managing remote changes to a git repository.
        """
        logging.info("Processing remote changes")
        self.fetch_and_merge()
