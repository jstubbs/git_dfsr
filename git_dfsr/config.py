"""
Read config files from:
- Deployed git_dfsr.conf inside package
- /etc/git_dfsr.conf

"""

import ConfigParser
import os

HERE = os.path.dirname(os.path.abspath((__file__)))

def read_config():
    parser = ConfigParser.ConfigParser()
    places = [os.path.abspath(os.path.join(HERE, '../git_dfsr.conf')),
              os.path.expanduser('/etc/git_dfsr.conf')]
    if not parser.read(places):
        raise RuntimeError("couldn't read config file from {0}"
                           .format(', '.join(places)))
    return parser

Config = read_config()