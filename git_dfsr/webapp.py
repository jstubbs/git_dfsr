"""
Listens for POST callbacks from the git remote when origin has changed. Updates the local git repository with fetch
and merge.
"""

import flask
import logging

from .config import Config

app_blueprint = flask.Blueprint('app_blueprint', __name__, url_prefix='/'+Config.get('app', 'endpoint'))

def create_app(git_repo, endpoint, secret):
    app = flask.Flask(__name__)
    app.config['endpoint'] = endpoint
    app.config['secret'] = secret
    app.config['git_repo'] = git_repo
    app.register_blueprint(app_blueprint)
    return app

@app_blueprint.route('/', methods=['POST'])
def process_update():
#    if not flask.current_app.config['secret'] == flask.request.form.get('secret'):
#        flask.abort(401)
    # return ("Would update this repo: " + flask.current_app.config['git_repo'].path)
    logging.info("Processing update request from remote")
    flask.current_app.config['git_repo'].remote_changes()
    logging.info("Local repository updated")
    return 'Repository updated.'
