"""
Watch class monitors a file system for changes (creations, updates and deltes) to files and takes actions on the
local git repository when necessary.

"""

import fnmatch
import logging
import pyinotify
import sys


class Watch(pyinotify.ProcessEvent):
    """
    Watch class monitors a file system for changes (creations, updates and deletes) to files and takes actions on the
    local git repository when necessary.

    """

    INOTIFY_EVENT_MASK = pyinotify.IN_CREATE | pyinotify.IN_DELETE | pyinotify.IN_MODIFY

    def __init__(self, git_repo, pattern, ignore_paths=None, start=False):
        # git repo object
        self.git_repo = git_repo
        # Unix shell-style pattern to match against the event's path
        self.pattern = pattern
        # paths to ignore in the pattern:
        self.ignore_paths = ['.git', '.swp']
        if ignore_paths and type(ignore_paths) == list:
            self.ignore_paths = self.ignore_paths + ignore_paths
        if start:
            self.loop()

    def path_match(self, path):
        """
        Check if a given patch matches this Watch instance's paths
        """
        for p in self.ignore_paths:
            if p in path:
                return False
        return fnmatch.fnmatch(path, self.pattern) or path == self.pattern

    def process_default(self, event):
        """
        Sole event processor for processing file system change events.
        """
        path = event.pathname.decode(sys.getfilesystemencoding())
        if self.path_match(path):
            self.git_repo.local_changes()

    def loop(self):
        """
        Set up the watchmanager and notifier with this watch and call the notifier loop.
        """
        logging.info("Starting watch of repo: " + str(self.git_repo.path))
        watch_manager = pyinotify.WatchManager()
        notifier = pyinotify.Notifier(watch_manager, self)
        watch_manager.add_watch(self.git_repo.path, Watch.INOTIFY_EVENT_MASK, rec=True, auto_add=True)
        notifier.loop()


