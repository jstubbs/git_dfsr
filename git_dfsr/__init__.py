"""

"""

from __future__ import absolute_import, print_function, unicode_literals
from argparse import ArgumentParser
import logging
from multiprocessing import Process
import os
import sys

from .config import Config
from .error import Error
from .gitrepo import GitRepo
from .watch import Watch
from .webapp import create_app


HERE = os.path.dirname(os.path.abspath((__file__)))
__version__ = open(os.path.join(HERE, 'VERSION')).read().strip()


def audit_config(path):
    # check that the git_repo exists on the local file system:
    if not os.path.exists(path):
        raise Error('path not found. Using: ' + str(path))
    # check that it is a .git repo:
    if not '.git' in os.listdir(path):
        raise Error('It does not look like ' + path + ' is actually a git repository.')


def main():
    logging.basicConfig(filename='../nohup.out', level=logging.INFO)
    parser = ArgumentParser(description="File system replication via git")
    parser.add_argument('-v', '--version', action='version', version=__version__)
    parser.add_argument('-p', '--path', help='absolute path to replicate.')
    parser.add_argument('-r', '--remote', help='URL to git remote.')
    parser.add_argument('-i', '--ignore_paths', help='relative paths and files to ignore.')
    parser.add_argument('-e', '--endpoint', help='name of the POST endpoint registered on the git remote.')
    parser.add_argument('-s', '--secret', help='token that must be passed in the message payload from the git remote.')
    parser.add_argument('-k', '--key', help='absolute path to private ssh key for accessing the git remote.')

    args = parser.parse_args()
    # check the congif object for options not specified on the command line:
    if not args.path:
        args.path = Config.get('local', 'path')
    if not args.remote:
        args.remote = Config.get('remote', 'remote')
    if not args.ignore_paths:
        args.ignore_paths = Config.get('local', 'ignore_paths')
    if not args.endpoint:
        args.endpoint = Config.get('app', 'endpoint')
    if not args.secret:
        args.secret = Config.get('app', 'secret')
    if not args.key:
        args.key = Config.get('remote', 'key')
    try:
        audit_config(args.path)
    except Error as e:
        logging.error("Invlaid config: " + str(e) + " Shutting down...")
        sys.exit(1)
    logging.info('Starting git_dfsr...')
    git_repo = GitRepo(args.path, args.remote, args.key)
    # start the web application
    app = create_app(git_repo, args.endpoint, args.secret)
    p1 = Process(target=app.run, kwargs={'host': '0.0.0.0'})
    p1.start()
    # start the watcher
    watch = Watch(git_repo, '*', ignore_paths=args.ignore_paths)
    p2 = Process(target=watch.loop)
    p2.start()
    # import pdb; pdb.set_trace()
    p1.join()
    p2.join()


if __name__ == '__main__':
    main()
