## git-dfs ##

Authors
-------

* Joe Stubbs <jstubbs@tacc.utexas.edu>
* Walter Moreira <wmoreira@tacc.utexas.edu>
* Rion Dooley <dooley@tacc.utexas.edu>
* Sam Clements <sam@borntyping.co.uk>