from __future__ import print_function
from setuptools import setup

import os
import sys


if sys.argv[-1] == 'publish':
    os.system('python setup.py sdist upload')
    sys.exit()

readme = open('README.md').read()

dependencies = []
with open('requirements.txt', 'r') as f:
    for line in f:
        dependencies.append(line)

setup(
    name='git_dfsr',
    version=open('git_dfsr/VERSION').read().strip(),
    description='Distributed File System Replication backed by Git',
    long_description=readme,
    author='Joe Stubbs',
    author_email='jstubbs@tacc.utexas.edu',
    url='https://bitbucket.org/jstubbs/git_dfsr',
    packages=[
        'git_dfsr'
    ],
    install_requires=dependencies,
    include_package_data=True,
    data_files=[('etc', ['git_dfsr.conf'])],
    license="MIT",
    zip_safe=False,
    keywords='git',
    classifiers=[
        'Development Status :: 1 - Pre-Alpha',
        'Intended Audience :: Developers',
        'License :: OSI Approved :: MIT License',
        'Natural Language :: English',
        "Programming Language :: Python :: 2",
        'Programming Language :: Python :: 2.7',
    ],
    test_suite='tests',
)
