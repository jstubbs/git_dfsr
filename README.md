# Git dfsr #

## Overview ##

Distributed file system replication backed by git.

Synchronize a file system path on a cluster of nodes using a remote git repository. See the Use Case section
below for information on performance and the intended use case.

The git_dfsr Ansible role (link needed) can be used to easily deploy git_dfsr to a cluster of nodes.

Don't use this project yet. It has no tests and doesn't work. On the other hand, contributions are welcome.


## Intended Use Case ##

There are lots of distributed file system solutions out there, but none that we could find that meet the specific needs
of our use case. Here is a high-level description of the requirements and assumptions:

1. The number of files stored on each node is large but the sizes of the files are small, mostly text/config files on
the order of a few 100K to couple of MBs range. This means that traditional "big data" solutions like HDFS are mostly
not appropriate.

2. Read/write performance: We want read performance to be fast at the cost of update performance (reads vastly outnumber
writes). In particular, we do not want reads to incur network latencies.

3. Homogeneity of nodes: every node must be able to make updates to the file system. Master-minion architectures such as
that used by Apache Helix are not workable for our use case.

4. Cluster unaware: in our environment, nodes are dynamically coming up and down to meet usage needs. This means it is
not possible for each node to be aware of every other node in the cluster at start up time, nor is it feasible to incur
restarts as a result of the cluster topology changing.

5. Highly available, fault tolerant and eventually consistent: because of the nature of our writes
(performed rarely by a vastly reduced subset of users) our reads (performed in high volumes), and our deployments (many
partitions arising from the use of numerous data centers) we trade consistency for availability and fault-tolerance (see
the CAP theorem). The underlying merge strategy used is very simple and will lead to data loss in many situations. As a
result of the strategy employed, given two nodes with conflicting data, the node that successfully pushes to the remote
first 'wins'. Given the nature of our updates this is an acceptable behavior.

6. Minimal install/maintenance: git_dfsr is a simple system built with minimal dependencies and is essentially
platform-agnostic. If you can run python and git on your linux servers you should be able to set up git_dfsr.

7. Canonical representation with history: In the event of catastrophic loss, the file system can be
restored on a new cluster from the canonical representation in the git remote. Similarly, a "development" cluster can be
created as a clone of production at any particular instant in time since the git repository maintains the full commit
history.


## Architecture ##

The git_dfsr system is simple, making use of just three components and a remote git server.

gitrepo.py: Python utility relying on GitPython (python binding for the git CLI) to manage the local git repository and
push and fetch updates to/from the remote.

watch.py: Python utility wrapping the pyinotify library; detects changes to the underlying file system.

webapp.py: Flask application listening for POST requests from the git remote to automatically fetch and merge updates.

Git server: The actual remote git repository. We use github because it is free and highly available.

## Setup ##

Install from source: Clone the repository, change into the project directory, activate a
virtualenv if appropriate and run:

```
#!bash

$ python setup.py install
```

Update the configs: the project includes an example config file in the root directory. Copy it to /etc/git_dfsr.conf and update as necessary. Important information includes: path to the local git repository, the URL for the remote, the path to the ssh key needed to communicate with the remote and 